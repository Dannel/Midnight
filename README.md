Midnight-gtk-theme
======
Midnight is a theme for GTK+ based desktop environments.  
It supports GTK+ 3, GTK+ 2, XFCE.

Requirements
------------
- GTK+ `>=3.20`
- `gnome-themes-standard`
- murrine engine

License
-------
Midnight is distributed under the terms of the GNU General Public License, version 2 or later. See the [`COPYING`](COPYING) file for details.

Special Thanks to
--------------
 - Nana-4, the developer of Flat-Plat.
 - juiceboxxxxxx, for the original concept.
 - godlyranchdressing
